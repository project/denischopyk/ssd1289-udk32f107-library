//-----------------------------------------------------------------------------
//    Заголовочный файл драйвера ЖКИ 320х240 на основе контроллера SSD1289
//-----------------------------------------------------------------------------
#ifdef __cplusplus
 extern "C" {
#endif
//-----------------------------------------------------------------------------
#ifndef _SSD1289
#define _SSD1289
//-----------------------------------------------------------------------------
//#include "stm32f10x_gpio.h"
//#include "stm32f10x_rcc.h"
#include "stm32f1xx_hal.h"


 typedef enum
{ Bit_RESET = 0,
  Bit_SET
}BitAction;




//-----------------------------------------------------------------------------
// не используются
#define BACKLIGHT_PORT           PORTD
#define BACKLIGHT_PIN            14
#define CS_PORT                  PORTC                // PC.9
#define CS_PIN                   9                // PC.9
#define WR_PORT                  PORTC                 // PC.6
#define WR_PIN                   6                // PC.6
#define RS_PORT                  PORTC               // PC.8
#define RS_PIN                   8                // PC.8
#define RD_PORT                  PORTD               // PD.15
#define RD_PIN                   15               // PD.15
#define DATA_PORT                PORTE              // PE

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//                       Обьявления функций
//-----------------------------------------------------------------------------
void LCD_Init();                                                                                 	    // Инициализация ЖКИ
void LCD_Write_REG(uint16_t Adr, uint16_t Data);                                                            // Загрузка данных в регистр ЖКИ
void LCD_Write_Command(uint8_t Comm);


uint16_t  LCD_Read_REG(uint16_t Adr);                                                                       // Чтение данных из регистра ЖКИ
void LCD_SetCursor(uint16_t x,uint16_t y);                                                                  // Установка курсора ЖКИ
void LCD_FillScreen(uint16_t Color);                                                                   // Заливка экрана одним цветом
void LCD_SetPoint(uint16_t x,uint16_t y,uint16_t Color);
uint16_t LCD_GetPoint(uint16_t x,uint16_t y);  // читает точку на ЖКИ
void LCD_WriteString_8x16(uint16_t x, uint16_t y, char *text, uint16_t charColor, uint16_t bkColor);                  // Вывод на ЖКИ текста шрифтом 8х16 пикселей
void LCD_WriteChar_8x16(uint16_t x, uint16_t y, char c, uint16_t t_color, uint16_t b_color);                          // Вывод на ЖКИ символа шрифтом 8х16 пикселей
void LCD_SetArea(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);                                                 // Выбор зоны для рисования
void LCD_WriteChar_5x7(uint16_t x, uint16_t y, char c, uint16_t t_color, uint16_t b_color, uint8_t rot, uint8_t zoom );         // Вывод на ЖКИ символа шрифтом 5х7 пикселей
void LCD_WriteString_5x7(uint16_t x, uint16_t y, char *text, uint16_t charColor, uint16_t b_color, uint8_t rot, uint8_t zoom ); // Вывод на ЖКИ текста шрифтом 5х7 пикселей
void LCD_Draw_Line(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2,uint16_t color);                                     // Вывод на ЖКИ линии
void LCD_Draw_Circle(uint16_t cx,uint16_t cy,uint16_t r,uint16_t color,uint8_t fill);
void Lcd_Circle(uint16_t  Xc, uint16_t Yc, uint16_t r, uint16_t color);
void LCD_Draw_Rectangle(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2,uint16_t color,uint8_t fill);                        // Вывод на ЖКИ прямоугольника
void LCD_Draw_Picture(uint16_t x0, uint16_t y0, const unsigned char *str);
void LCD_Draw_Picture2(uint16_t x0, uint16_t y0, const uint16_t *img);
void LCD_Draw_BMP(uint16_t x0, uint16_t y0, const unsigned int *str, uint8_t clck);
void LCD_WriteDataMultiple(uint16_t * pData, int NumItems);
void LCD_ReadDataMultiple(uint16_t * pData, int NumItems);
void LCD_Write_Data(uint16_t Data);               // Отправка данных ЖКИ
uint16_t  LCD_Read_Data();                        // Чтение данных из ЖКИ
//-----------------------------------------------------------------------------
//                       Коды цветов
//-----------------------------------------------------------------------------
#define BLACK                0x0000
#define WHITE                0xFFFF
#define GRAY                 0xE79C
#define GREEN                0x07E0
#define BLUE                 0x001F
#define RED                  0xF800
#define SKY                  0x5D1C
#define YELLOW               0xFFE0
#define MAGENTA              0xF81F
#define CYAN                 0x07FF
#define ORANGE               0xFCA0
#define PINK                 0xF97F
#define BROWN                0x8200
#define VIOLET               0x9199
#define SILVER               0xA510
#define GOLD                 0xA508
#define BEGH                 0xF77B
#define NAVY		     0x000F
#define DARK_GREEN	     0x03E0
#define DARK_CYAN	     0x03EF
#define MAROON		     0x7800
#define PURPLE		     0x780F
#define OLIVE		     0x7BE0
#define LIGHT_GREY	     0xC618
#define DARK_GREY	     0x7BEF
//-----------------------------------------------------------------------------
#endif /* _SSD1289 */
//-----------------------------------------------------------------------------
#ifdef __cplusplus
}
#endif /* _cplusplus*/
//-----------------------------------------------------------------------------
