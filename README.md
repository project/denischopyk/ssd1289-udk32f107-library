Библиотека адаптирована для использования совместно с HAL. Управление контроллером индикатора (SSD1289) осуществляется посредством 16-разрядной параллельной шины. ЖКИ позволяет отображать графические данные с разрешением 320х240 пикселей до 262000 цветов. В состав модуля индикатора входит резистивный Touchscreen с контроллером XPT2046. Учитывая то, что микросхема XPT2046 не поддерживает высокоскоростного обмена по шине SPI, Touch Screen управляется посредством программно организованного SPI-интерфейса, что позволяет освободить аппаратный SPI2 для работы с более быстрыми устройствами, такими, как Dataflash и microSD card.


В конструкцию отладочной платы введена возможность управления подсветкой TFT-матрицы посредством подачи уровня лог.1 на вывод BLCNT разъема модуля ЖКИ.
Если нет нyжды использовать ЖКИ, то сигналы управления пользователь может использовать для других нужд по своему усмотрению, получив таким образом на плате два разъема расширения вместо одного.

![Alt Text](https://dahl-device.ru/wp-content/uploads/2022/04/hy32d.png)
